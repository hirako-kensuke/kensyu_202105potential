<!DOCTYPE html>
<?php
    include('include/include_text.php');
 ?>
<html>
    <head>
        <link rel='stylesheet' type='text/css' href='include/style.css'>
      <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>新規社員登録画面</title>
        <script>

        </script>
    </head>
    <body>

        <table border="0" style="width:100%">
          <tr>
            <td id='header-title'>社員名簿システム</td>
            <td id='header-link'>|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>
          </tr>
        </table>
        <hr>
        <form method='POST' action='entry02.php'>
            <div class='s-result d-result' id='table'>
                <table border='1' style='border-collapse:collapse;'>
                    <tbody>
                        <tr>
                            <th>
                                名前
                            </th>
                            <td>
                                <label><input type='text' name='name' value=''required></label>
                            </td>
                        </tr>
                        <tr>
                            <th>出身地</th>
                            <td>
                                <select name='pref'>
                                <?php
                                foreach($pref_array as $each => $value) {
                                    echo '<option value="', $each, '">', $value, '</option>';
                                }
                                ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td>
                                <label><input type='radio' name='seibetu' value='1' checked>男</label>
                                <label><input type='radio' name='seibetu' value='2' >女</label>
                            </td>
                        </tr>
                        <tr>
                            <th>年齢</th>
                            <td>
                                <input type='number' name='age' value=''min='01' max="99"required>才
                            </td>
                        </tr>
                        <tr>
                            <th>所属部署</th>
                            <td>
                                <label><input type='radio' name='section_ID' value='1' checked>第一事業部</label>
                                <label><input type='radio' name='section_ID' value='2'>第二事業部</label>
                                <label><input type='radio' name='section_ID' value='3'>営業</label>
                                <label><input type='radio' name='section_ID' value='4'>総務</label>
                                <label><input type='radio' name='section_ID' value='5'>人事</label>
                            </td>
                        </tr>
                        <tr>
                            <th>役職</th>
                            <td>
                                <label><input type='radio' name='grade_ID' value='1' checked>事業部長</label>
                                <label><input type='radio' name='grade_ID' value='2'>部長</label>
                                <label><input type='radio' name='grade_ID' value='3'>チームリーダー</label>
                                <label><input type='radio' name='grade_ID' value='4'>リーダー</label>
                                <label><input type='radio' name='grade_ID' value='5'>メンバー</label>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class='textright'>
                <input type='submit' value='登録' onclick=''>
                <input type='reset' value='リセット'>
            </div>
        </form>
    </body>
</html>
