<!DOCTYPE html>
<?php

    include('include/include_text.php');

    $where_str = "";
    $cond_name = "";
    $cond_seibetu = "";
    $cond_section_name = "";
    $cond_grade_name = "";

    if (isset($_GET['name']) && !empty($_GET['name'])){
        $where_str .= " AND name LIKE '%" . $_GET['name'] . "%'";
        $cond_name = $_GET['name'];
    }
    if (isset($_GET['seibetu']) && !empty($_GET['seibetu'])){
        $where_str .= " AND seibetu = '" . $_GET['seibetu'] . "'";
        $cond_seibetu = $_GET['seibetu'];
    }
    if (isset($_GET['section_name']) && !empty($_GET['section_name'])){
        $where_str .= " AND section1_master.ID = '" . $_GET['section_name'] . "'";
        $cond_section_name = $_GET['section_name'];
    }
    if (isset($_GET['grade_name']) && !empty($_GET['grade_name'])){
        $where_str .= " AND grade_master.ID = '" . $_GET['grade_name'] . "'";
        $cond_grade_name = $_GET['grade_name'];
    }

    $query_str = "SELECT member.member_ID, member.name, section1_master.section_name, grade_master.grade_name
            	FROM `member`
            	LEFT JOIN section1_master ON section1_master.ID = member.section_ID
            	LEFT JOIN grade_master ON grade_master.ID = member.grade_ID
                WHERE 1";

    $query_str .= $where_str;

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchall();
?>
<html>
    <head>
        <link rel='stylesheet' type='text/css' href='include/style.css'>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>

        <title>社員名簿システム</title>
        <script type='text/javascript'>
        <!--
        //フォームのリセット
        function clearForm(){
            document.searchform.name.value = "";
            document.searchform.seibetu.value = "0";
            document.searchform.section_name.value = "0";
            document.searchform.grade_name.value = "0";
        }
        -->
        </script>
    </head>
    <body>
    <table border="0" style="width:100%">
      <tr>
        <td id='header-title'>社員名簿システム</td>
        <td id='header-link'>|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>
      </tr>
    </table>
        <hr>
        <form name='searchform' method='get' action='index.php'>
            <div id='form-search'>
            <table border='0' align="center">
                <tr>
                    <th>
                        名前
                    </th>
                    <td colspan='5'>
                        <input type='text' name='name' maxlength="30" value='<?php if(!empty($_GET['name'])) echo $_GET['name'];?>'>
                    </td>
                </tr>
                <tr>
                    <th>
                        性別
                    </th>
                    <td>
                        <select name='seibetu'>
                            <option value='0' <?php if(empty($_GET['seibetu'])) echo 'selected';?>>
                                すべて
                            </option>
                            <option value='1' <?php if(isset($_GET['seibetu']) and $_GET['seibetu'] == '1') { echo 'selected';}?>>
                                男性
                            </option>
                            <option value='2' <?php if(isset($_GET['seibetu']) and $_GET['seibetu'] == '2') { echo 'selected';}?>>
                                女性
                            </option>
                        </select>
                    </td>
                    <th>
                        部署
                    </th>
                    <td>
                        <select name='section_name'>
                            <option value='0' <?php if(empty($_GET['section_name'])) echo 'selected';?>>
                                すべて
                            </option>
                            <option value='1' <?php if(isset($_GET['section_name']) and $_GET['section_name'] == '1') { echo 'selected';}?>>
                                第一事業部
                            </option>
                            <option value='2' <?php if(isset($_GET['section_name']) and $_GET['section_name'] == '2') { echo 'selected';}?>>
                                第二事業部
                            </option>
                            <option value='3' <?php if(isset($_GET['section_name']) and $_GET['section_name'] == '3') { echo 'selected';}?>>
                                営業
                            </option>
                            <option value='4' <?php if(isset($_GET['section_name']) and $_GET['section_name'] == '4') { echo 'selected';}?>>
                                総務
                            </option>
                            <option value='5' <?php if(isset($_GET['section_name']) and $_GET['section_name'] == '5') { echo 'selected';}?>>
                                人事
                            </option>
                        </select>
                    </td>
                    <th>
                        役職
                    </th>
                    <td>
                        <select name='grade_name'>
                            <option value='0' <?php if(empty($_GET['grade_name'])) echo 'selected';?>>
                                すべて
                            </option>
                            <option value='1' <?php if(isset($_GET['grade_name']) and $_GET['grade_name'] == '1') { echo 'selected';}?>>
                                事業部長
                            </option>
                            <option value='2' <?php if(isset($_GET['grade_name']) and $_GET['grade_name'] == '2') { echo 'selected';}?>>
                                部長
                            </option>
                            <option value='3' <?php if(isset($_GET['grade_name']) and $_GET['grade_name'] == '3') { echo 'selected';}?>>
                                チームリーダー
                            </option>
                            <option value='4' <?php if(isset($_GET['grade_name']) and $_GET['grade_name'] == '4') { echo 'selected';}?>>
                                リーダー
                            </option>
                            <option value='5' <?php if(isset($_GET['grade_name']) and $_GET['grade_name'] == '5') { echo 'selected';}?>>
                                メンバー
                            </option>
                        </select>
                    </td>
                </tr>
            </table>
            </div>
            <div class='textcenter'>
            <input type='submit' value='検索'>
            <input type='button' value='リセット' onClick='clearForm();'>
            </div>
        </form>
        <hr>
        <div class='s-result' id='table'>
            検索結果：
            <?php echo count($result);?>
            <table>
                <tr>
                    <th>
                        社員ID
                    </th>
                    <th>
                        名前
                    </th>
                    <th>
                        部署
                    </th>
                    <th>
                        役職
                    </th>
                </tr>
                <?php
                    if (count($result) == 0){
                        echo "<tr><td colspan='4' align='center'>検索結果なし</td></tr>";
                    } else {
                            foreach($result as $each){
                                echo "<tr><td>" . $each['member_ID'] . "</td>"
                                . "<td>" . "<a href='detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>"
                                . "<td>" . $each['section_name'] . "</td>"
                                . "<td>" . $each['grade_name'] . "</td>"
                                ."</tr>";
                            }
                    }
                ?>
            </table>
        </div>
    </body>
</html>
