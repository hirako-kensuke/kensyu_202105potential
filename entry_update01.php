<!DOCTYPE html>
<?php
    include('include/include_text.php');

    // common
    // include('./include/functions.php');
    $DB_DSN = 'mysql:host=localhost; dbname=msasaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    $where_str = "";
    $cond_member_ID = "";
    $cond_name='';
    $cond_pref='';
    $cond_seibetu='';
    $cond_age='';
    $cond_section_ID='';
    $cond_grade_ID='';


    if (isset($_GET['member_ID']) && !empty($_GET['member_ID'])){
        $where_str .= " AND member_ID = " . $_GET['member_ID'];
        $cond_member_ID = $_GET['member_ID'];
    }


    //$query_str = 'SELECT * FROM member WHERE ';


    $query_str = "SELECT * FROM member WHERE 1 ";

    $query_str.=$where_str;

    //echo $query_str;

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchALL();
    ?>
    <!-- <pre>
    <?php
    var_dump($result);
    ?>
    </pre> -->


<html>
    <head>

        <link rel='stylesheet' type='text/css' href='include/style.css'>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>既存社員情報修正画面</title>

        <script>
        function goEdit(id){
            if(window.confirm('更新を行います。よろしいですか？')){

               //  tempstr = "./entry_update02.php?member_ID=" + id;
               //
    	       // location.href = tempstr;

               document.updateform.submit();
            }
        }
        </script>
    </head>
    <body>

        <form method='GET' action='entry_update02.php' name='updateform'>
            <table border="0" style="width:100%">
              <tr>
                <td id='header-title'>社員名簿システム</td>
                <td id='header-link'>|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>
              </tr>
            </table>
         <hr>
         <div class='s-result d-result' id='table'>
             <table border='1' style='border-collapse:collapse;'>
                <tr>
                    <th>社員ID</th>
                    <td>
                        <?php
                        if (!empty($_GET['member_ID'])) {
                            $member_ID = $_GET['member_ID'];
                            if ('member_ID' == "") {
                                //echo $_GET['member_ID'];
                            } else {
                                echo $member_ID;

                            }
                        }?>
                    </td>
                </tr>
                <tr>
                    <th>名前</th>
                    <td>
                    <input type="text" name='name' maxlength="30" value='<?php echo $result[0]['name'];?>'required>
                    </td>
                </tr>
                <tr>
                    <th>出身地</th>
                    <td>
                        <select name='pref'>
                            <?php
                                foreach($pref_array as $key => $value) {
                                    echo "<option value='" . $key . "'";
                                    if($result[0]['pref'] == $key){ echo "selected";}
                                    echo ">" . $value .  "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>性別</th>
                    <td>
                        <label><input type='radio' name='seibetu' value='1'<?php echo $result[0]['seibetu']=='1' ? 'checked':""?>>男</label>
                        <label><input type='radio' name='seibetu' value='2'<?php echo $result[0]['seibetu']=='2' ? 'checked':""?>>女</label>
                    </td>
                </tr>
                <tr>
                    <th>年齢</th>
                    <td>
                        <input type='number' name='age' value='<?php echo $result[0]['age'];?>'min='01' max="99" required>才
                    </td>
                </tr>
                <tr>
                    <th>所属部署</th>
                    <td>
                        <label><input type='radio' name='section_ID' value='1' <?php echo $result[0]['section_ID'] =='1' ? 'checked':""?>>第一事業部</label>
                        <label><input type='radio' name='section_ID' value='2' <?php echo $result[0]['section_ID'] =='2' ? 'checked':""?>>第二事業部</label>
                        <label><input type='radio' name='section_ID' value='3' <?php echo $result[0]['section_ID'] =='3' ? 'checked':""?>>営業</label>
                        <label><input type='radio' name='section_ID' value='4' <?php echo $result[0]['section_ID'] =='4' ? 'checked':""?>>総務</label>
                        <label><input type='radio' name='section_ID' value='5' <?php echo $result[0]['section_ID'] =='5' ? 'checked':""?>>人事</label>
                    </td>
                </tr>
                <tr>
                    <th>役職</th>
                    <td>
                        <label><input type='radio' name='grade_ID' value='1' <?php echo $result[0]['grade_ID'] =='1' ? 'checked':""?>>事業部長</label>
                        <label><input type='radio' name='grade_ID' value='2' <?php echo $result[0]['grade_ID'] =='2' ? 'checked':""?>>部長</label>
                        <label><input type='radio' name='grade_ID' value='3' <?php echo $result[0]['grade_ID'] =='3' ? 'checked':""?>>チームリーダー</label>
                        <label><input type='radio' name='grade_ID' value='4' <?php echo $result[0]['grade_ID'] =='4' ? 'checked':""?>>リーダー</label>
                        <label><input type='radio' name='grade_ID' value='5' <?php echo $result[0]['grade_ID'] =='5' ? 'checked':""?>>メンバー</label>
                    </td>
                </tr>
            </table>
        </div>
        <div class='textright'>
            <input type='button' value='登録' onclick='goEdit(<?php echo $cond_member_ID; ?>)'>
            <input type='reset' value='リセット'>
            <input type='hidden' name='member_ID' value = '<?php echo $member_ID;?>'>
        </div>
        </form>
    </body>
</html>
