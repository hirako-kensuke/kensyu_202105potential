<!DOCTYPE html>
<?php
    include('include/include_text.php');

    $DB_DSN = 'mysql:host=localhost; dbname=msasaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN,$DB_USER,$DB_PW);


    $where_str = "";
    $cond_member_ID = "";
    $cond_name = "";

    //echo $_GET['member_ID'];


    if (isset($_GET['member_ID']) && !empty($_GET['member_ID'])){
        $where_str .= " AND member_ID = " . $_GET['member_ID'];
        $cond_member_ID = $_GET['member_ID'];
    }else{
        echo 'エラーが発生しました。トップ画面に戻ってください';
    }


    $query_str = 'SELECT member.member_ID,member.name,member.pref,member.seibetu,member.age,section1_master.section_name,grade_master.grade_name FROM member
    left join section1_master on member.section_ID=section1_master.ID
    left join grade_master on member.grade_ID=grade_master.ID WHERE 1  ' ;

    $query_str .= $where_str;

    //echo $query_str;

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchALL();

    //var_dump($result);

    if(count($result=='1')){
            //正常
        }else{
            echo 'エラーが発生しました。トップ画面に戻ってください';

    }
?>
<html>
<head>
    <meta charset='utf-8'>
    <link rel='stylesheet' type='text/css' href='include/style.css'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

        <title>社員名簿システム</title>
        <script>
        function goDel(id){
                if(window.confirm('削除を行います。よろしいですか？')){
            // location.href = "./entry_update01.php?member_ID=" + id;
            tempstr = "./delete01.php?member_ID=" + id;
    	//alert(tempstr);
    	   location.href = tempstr;
            }
        }


        function goEdit(id){

            // location.href = "./entry_update01.php?member_ID=" + id;
            tempstr = "./entry_update01.php?member_ID=" + id;
    	//alert(tempstr);
    	   location.href = tempstr;
        }
        </script>
    </head>
    <body>
        <form method="GET" action="entry_update01.php">
            <table border="0" style="width:100%">
              <tr>
                <td id='header-title'>社員名簿システム</td>
                <td id='header-link'>|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>
              </tr>
            </table>
            <hr>
            <div class='s-result d-result' id='table'>
                <table border="1" align="center">
                    <tr>
                        <th>
                            社員ID
                        </th>
                        <td>
                            <?php echo $result[0]['member_ID'];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            名前
                        </th>
                        <td>
                            <?php echo $result[0]['name'];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            出身地
                        </th>
                        <td>
                            <?php echo $pref_array[$result[0]['pref']];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            性別
                        </th>
                        <td>
                            <?php echo $gender_array[$result[0]['seibetu']];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            年齢
                        </th>
                        <td>
                            <?php echo $result[0]['age'];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            所属部署
                        </th>
                        <td>
                            <?php echo $result[0]['section_name'];?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            役職
                        </th>
                        <td>
                            <?php echo $result[0]['grade_name'];?>
                        </td>
                    </tr>
                </table>
            </div>
            <hr>

            <div class='textright'>
                <input type='button' value='編集' onclick='goEdit(<?php echo $cond_member_ID ?>);'>
                <input type='button' value='削除' onclick='goDel(<?php echo $cond_member_ID ?>);'>
            </div>
        </form>
    </body>
</html>
